import sys
from faker import Faker
import csv

output_dir = sys.argv[1]
output = open("{}/people.csv".format(output_dir), 'w', newline='')
fake = Faker()
header = ['name', 'age', 'street', 'city', 'state', 'zip', 'lng', 'lat']
mywriter = csv.writer(output)
mywriter.writerow(header)
for r in range(1000):
    mywriter.writerow([fake.name(), fake.random_int(min=15,
                                                    max=90, step=1), fake.street_address(), fake.city(), fake.
                       state(), fake.zipcode(), fake.longitude(), fake.latitude()])
output.close()
print({"status": "Complete"})
