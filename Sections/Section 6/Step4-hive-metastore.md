#### Run SQL on CSV files

```bash
$ docker-compose -f ./docker-compose-hive.yml up
$ docker exec -it namenode bash
$ hadoop fs -mkdir -p /data/data_lake/stock
$ hadoop fs -put /data/Mar* /data/data_lake/stock

$  hadoop fs -ls -h -R /data
drwxr-xr-x   - root supergroup          0 2021-03-02 20:11 /data/data_lake
drwxr-xr-x   - root supergroup          0 2021-03-02 20:13 /data/data_lake/stock
-rw-r--r--   1 root supergroup    116.8 K 2021-03-02 20:13 /data/data_lake/stock/MarketWatchPlus-1399_11_19.csv
-rw-r--r--   1 root supergroup    116.5 K 2021-03-02 20:13 /data/data_lake/stock/MarketWatchPlus-1399_11_20.csv
-rw-r--r--   1 root supergroup    112.9 K 2021-03-02 20:13 /data/data_lake/stock/MarketWatchPlus-1399_11_21.csv
```



#### Define External Table in Hive 

run `hdfs_data\hive.hql` in DBeaver after creating a connection to Hive.



#### Hive Catalog

- copy  `hdfs_data\hive.properties` to `catalog` folder & restart the cluster ...
- in DBeaver , query CSV files! 



#### Trino-Cli

```bash
$ docker exec -it coordinator bash

$ trino-cli
trino> use hive.default;
trino:default> select *
            -> from hive."default".stock_exchange_daily
            -> where symbol like  'فاراك'
            -> order by last_order_value_change_percent desc ;
            
             symbol |    full_name    | quantity |  volume  | value | yesterday_qnt | first_order_value | last_order_value | last_order_value_change | last_order_value_
--------+-----------------+----------+----------+-------+---------------+-------------------+------------------+-------------------------+------------------
 فاراك  | ماشين<U+200C> سازي<U+200C> اراك<U+200C> |     3272 | 39386315 |  NULL |          5000 |              4990 |             5250 |                   2
 فاراك  | ماشين<U+200C> سازي<U+200C> اراك<U+200C> |     1938 | 24361108 |  NULL |          4900 |              4810 |             5140 |                   2
 فاراك  | ماشين<U+200C> سازي<U+200C> اراك<U+200C> |     3793 | 42018919 |  NULL |          5120 |              5300 |             5020 |                  -1
            
```



#### Sample Queries

```sql
select * 
from hive."default".stock_exchange_daily
where last_order_value_change_percent > 4
order by last_order_value_change_percent desc ;
; 


select *
from hive."default".stock_exchange_daily
where symbol like  'فا%'
 ;
```



