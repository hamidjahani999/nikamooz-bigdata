#### Section 1 : Intro Presentation



#### Section 2 :  Custom Python Script 

***topics*** : 

- Running Custom **Python Scripts**

- ***topics*** : 

  - Process flow files with python

  ***Docker Compose***  :  docker-compose-sahamyab.yml

#### Section 3 : Sahamyab  -  Elastic Search Output

***topics*** : 

- Elasticsearch/Kibana

***Docker Compose***  :  `docker-compose-sahamyab-es.yml`

**Guide** : 

- `Section3-Sahamyab-V4-Save-To-Elasticsearch`

#### Section 4 : Sahamyab  -  Kafka Producer, Consumer

***topics*** : 

- Kafka

***Docker Compose***  :  `docker-compose-sahamyab-kafka.yml`

**Guide** : 

- `Section4-Sahamyab-V5-Send-To-Kafka.md`



#### Section 5 : Some Advanced Topics 

***topics*** : 

- Registry
- Working With Colors
- Variables
- Back Pressure
- Revert Changes
- Input/Output Ports

***Inspired By*** : [Setting Apache Nifi on Docker Containers](https://medium.com/analytics-vidhya/setting-apache-nifi-on-docker-containers-a00e862a8399)

***Docker Compose***  :  `docker-compose-registry.yml`

***Nifi Template*** : 

- `Section5-A-SimpleFlow.xml`
- `Section5-B-Split-Flow.xml`

***Further Reading :***  The Book *"Data Engineering with Python"* : Chapter 10: Deploying Data Pipelines



#### Section 6 : A practical ETL Pipeline

***topics*** : 

- Ensure Data Quality Using [GreatExpectations](https://greatexpectations.io/)

- Generate Fake Data 

- Counters

- Parameter Context

- Git Backend Registry

- Reporting Tasks

  

***Docker Compose***  :  `docker-compose-registry.yml`

***Nifi TEmplate*** : `Section6-Data-Pipeline.xml`

***Further Reading :***  

- The Book *"Data Engineering with Python"* : Chapter 7: Features of a Production Pipeline
- [Choose Your Adventure: Exploring Great Expectations Datasources and Batch Kwargs | by Great Expectations | Medium](https://medium.com/@expectgreatdata/choose-your-adventure-exploring-great-expectations-datasources-and-batch-kwargs-290cb9c08220)
- [NiFi System Administrator’s Guide (apache.org)](https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html#clustering)



