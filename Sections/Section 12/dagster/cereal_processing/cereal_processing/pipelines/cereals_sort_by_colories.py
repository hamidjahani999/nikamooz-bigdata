from dagster import ModeDefinition, pipeline
from cereal_processing.solids.step_2 import load_cereals, sort_by_calories

MODE_DEV = ModeDefinition(name="dev", resource_defs={})
MODE_TEST = ModeDefinition(name="test", resource_defs={})

@pipeline(mode_defs=[MODE_DEV, MODE_TEST])
def cereals_sort_by_colories():
    sort_by_calories(load_cereals())