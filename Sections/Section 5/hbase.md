HBase Shell

run docker containers :

```bash
$ docker-compose up

```

in another tab,  execute :

```bash
$  docker exec -it hbase-master bash
$ hbase shell

```

 ##### HBase Commands 

**getting started ....**

```bash
$ status
1 active master, 0 backup masters, 1 servers, 0 dead, 2.0000 average load
Took 1.4765 seconds
$ help "status"
Show cluster status. Can be 'summary', 'simple', 'detailed', or 'replication'. The
default is 'summary'. Examples:

  hbase> status
  hbase> status 'simple'
  hbase> status 'summary'
  hbase> status 'detailed'
  hbase> status 'replication'
  hbase> status 'replication', 'source'
  hbase> status 'replication', 'sink' 
$ version
2.1.3, rda5ec9e4c06c537213883cca8f3cc9a7c19daf67, Mon Feb 11 15:45:33 CST 2019
Took 0.0008 seconds
$  table_help
Help for table-reference commands.

You can either create a table via 'create' and then manipulate the table via commands like 'put', 'get', etc.
See the standard help information for how to use each of these commands.

However, as of 0.96, you can also get a reference to a table, on which you can invoke commands.
For instance, you can get create a table and keep around a reference to it via:

   hbase> t = create 't', 'cf'
 ...
 
 $ 
```



#### Tables Managements commands

These commands will allow programmers to create tables and table schemas with rows and column families.

The following are Table Management commands

- Create
- List
- Describe
- Disable
- Disable_all
- Enable
- Enable_all
- Drop
- Drop_all
- Show_filters
- Alter
- Alter_status

```bash
$ create 'user', 'info', 'clicks', 'orders'
Created table user
Took 0.9978 seconds
=> Hbase::Table - user

$ list
TABLE
user
1 row(s)
Took 0.0267 seconds
=> ["user"]

$ describe 'user'
Table user is ENABLED
user
COLUMN FAMILIES DESCRIPTION
{NAME => 'clicks', VERSIONS => '1', EVICT_BLOCKS_ON_CLOSE => 'false', NEW_VERSION_BEHAVIOR => 'false', KEEP_DELETED_CELLS => 'FALSE',
 CACHE_DATA_ON_WRITE => 'false', DATA_BLOCK_ENCODING => 'NONE', TTL => 'FOREVER', MIN_VERSIONS => '0', REPLICATION_SCOPE => '0', BLOO
MFILTER => 'ROW', CACHE_INDEX_ON_WRITE => 'false', IN_MEMORY => 'false', CACHE_BLOOMS_ON_WRITE => 'false', PREFETCH_BLOCKS_ON_OPEN =>
 'false', COMPRESSION => 'NONE',
 ....
 
 $ disable 'user'
 Took 1.3720 seconds
 
 $ disable_all ".*"
user

Disable the above 1 tables (y/n)?
y
1 tables successfully disabled
Took 3.1020 seconds
 
 $ enable 'user'
 Took 0.7527 seconds
 
 $ enable_all "user*" 
 ...
 
 
 $ show_filters
DependentColumnFilter
KeyOnlyFilter
ColumnCountGetFilter
SingleColumnValueFilter
PrefixFilter
SingleColumnValueExcludeFilter
FirstKeyOnlyFilter
ColumnRangeFilter
ColumnValueFilter
TimestampsFilter
FamilyFilter
...
 
 $ drop 'user'
ERROR: Table user is enabled. Disable it first.
For usage try 'help "drop"'
Took 0.0247 seconds

$ drop_all "user*"

$ is_enabled 'user'
true
Took 0.0078 seconds
=> true


```



##### Alter Command

**Syntax** :  `alter <tablename>, {property => value} as in desc 'table_name' `

```bash
$ alter 'user' , {NAME => 'info', VERSIONS=>5}
Updating all regions with the new schema...
1/1 regions updated.
Done.
Took 1.8160 seconds

$ alter 'user' , {NAME => 'comments', VERSIONS=>3}
Updating all regions with the new schema...
1/1 regions updated.
Done.

$ alter 'user' , {NAME => 'comments', METHOD => 'delete'}
...

$ alter 'user', 'delete' => 'comments'

...

###  table-scope attributes : MAX_FILESIZE, READONLY, MEMSTORE_FLUSHSIZE, DEFERRED_LOG_FLUSH
$ alter 'user', MAX_FILESIZE=>'132545224'

$ alter 'user', METHOD => 'table_att_unset', NAME => 'MAX_FILESIZE'
...

$ alter_status 'user'


```



## Data manipulation commands

These commands will work on the table related to data manipulations such as putting data into a table, retrieving data from a table and deleting schema, etc.

The commands come under these are

- Count
- Put
- Get
- Delete
- Delete all
- Truncate
- Scan

```bash
$ count 'user', CACHE=>1000
0 row(s)
Took 0.2470 seconds

$ put 'user','u230', 'info:name' ,'ali'
$ put 'user','u230', 'info:family' ,'ahmadi'
$ put 'user','u123', 'info:name' ,'sara'
$ put 'user','u123', 'info:family' ,'zarrabi'
$ put 'user','u56', 'info:name' ,'kamran'
$ put 'user','u56', 'info:family' ,'heravi'

$ count 'user', INTERVAL =>2, CACHE=> 10
Current count: 2, row: u230
3 row(s)
Took 0.0180 seconds
=> 3



$ scan 'user'
ROW                                COLUMN+CELL
 u123                              column=info:family, timestamp=1611089015045, value=zarrabi
 u123                              column=info:name, timestamp=1611088989690, value=sara
 u230                              column=info:family, timestamp=1611088966681, value=ahmadi
 u230                              column=info:name, timestamp=1611088947650, value=ali
 u56                               column=info:family, timestamp=1611089088075, value=heravi
 u56                               column=info:name, timestamp=1611089060227, value=kamran
3 row(s)

$ put 'user','u56', 'clicks:about.html' ,'home.html'
$ put 'user','u56', 'clicks:contact.html' ,'about.html'

$ get 'user', 'u123'
COLUMN                             CELL
 info:family                       timestamp=1611089015045, value=zarrabi
 info:name                         timestamp=1611088989690, value=sara
1 row(s)
Took 0.0229 seconds


$  get 'user', 'u56','clicks'
COLUMN                             CELL
 clicks:about.html                 timestamp=1611089512387, value=home.html
 clicks:contact.html

$ get 'user', 'u56', { TIMERANGE => [1611089512387, 1611089512500] }
COLUMN                             CELL
 clicks:about.html                 timestamp=1611089512387, value=home.html
1 row(s)
Took 0.0517 seconds

$ get 'user', 'u56', { COLUMNS => ['clicks','orders'] }
COLUMN                             CELL
 clicks:about.html                 timestamp=1611089512387, value=home.html
 clicks:contact.html               timestamp=1611089527137, value=about.html
1 row(s)

$  get 'user', 'u56', 'clicks','orders'
COLUMN                             CELL
 clicks:about.html                 timestamp=1611089512387, value=home.html
 clicks:contact.html               timestamp=1611089527137, value=about.html
1 row(s)
Took 0.0124 seconds

$ delete 'user','u56', 'clicks:contact.html'

$ put 'user','u56', 'clicks:contact.html' , 'about.html'
$ put 'user','u56', 'clicks:contact.html' , 'about2.html'

$ get 'user', 'u56', 'clicks','orders'
COLUMN                             CELL
 clicks:about.html                 timestamp=1611089512387, value=home.html
 clicks:contact.html               timestamp=1611090140639, value=about2.html
1 row(s)
Took 0.0226 seconds

$ help 'get'


$  get  'user', 'u56', {COLUMN => 'clicks', VERSIONS => 2}
COLUMN                             CELL
 clicks:about.html                 timestamp=1611089512387, value=home.html
 clicks:contact.html               timestamp=1611090649090, value=about3.html
1 row(s)
Took 0.0150 seconds

$ desc 'user'
$ alter 'user', {NAME=>'clicks' , VERSIONS => 5}
$ put 'user','u56', 'clicks:contact.html' , 'about3.html'

$ get  'user', 'u56', {COLUMN => 'clicks', VERSIONS => 2}
COLUMN                             CELL
 clicks:about.html                 timestamp=1611089512387, value=home.html
 clicks:contact.html               timestamp=1611091009472, value=about3.html
 clicks:contact.html               timestamp=1611090993084, value=about.html
 
$ delete 'user','u56', 'clicks:contact.html'
Took 0.0067 seconds
    $ get  'user', 'u56', {COLUMN => 'clicks', VERSIONS => 2}
COLUMN                             CELL
 clicks:about.html                 timestamp=1611089512387, value=home.html
 clicks:contact.html               timestamp=1611090649090, value=about3.html

$ deleteall 'user','u56', 'clicks:contact.html'

$ help 'scan' 


$ truncate 'user'

 
```



#### HappyBase

```python
import happybase

connection = happybase.Connection('127.0.0.1')
table = connection.table('user')

table.put(b'row-key', {b'family:qual1': b'value1',
                       b'family:qual2': b'value2'})

row = table.row(b'row-key')
print(row[b'family:qual1'])  # prints 'value1'

for key, data in table.rows([b'row-key-1', b'row-key-2']):
    print(key, data)  # prints row key and data for each row

for key, data in table.scan(row_prefix=b'row'):
    print(key, data)  # prints 'value1' and 'value2'

row = table.delete(b'row-key')
```

