#### Install Dagster

```bash
$ mkdir dagster
$ cd dagster
$ python -m venv .venv
$ .\.venv\Scripts\activate
$ pip install dagster dagit pytest
```

#### Hello Dagster!

create a python file and name it `hello_world.py` 

```python
from dagster import pipeline, solid

@solid
def get_name(_):
    return "dagster"

@solid
def hello(context, name: str):
    context.log.info(f"Hello, {name}!")


@pipeline
def hello_pipeline():
    hello(get_name())
```

- open the file with VS Code : `code .` & set the proper interpreter.

- run the ui : `dagit -f hello_world.py`

- add `str` type to `get_name` function : 

  ```python
  @solid
  def get_name(_) -> str:
      return "dagster"
  ```

- stop & start the pipeline

#### Other Options to Run Dagster Pipelines

**Python Code :**

- add following snippet to the `hello_world.py` and execute it :

```python
from dagster import execute_pipeline

if __name__ == "__main__":
    result = execute_pipeline(hello_pipeline)
```

**Dagster CLI**

```bash
dagster pipeline execute -f hello_world.py
```



#### Create-new-project

- follow official  documents : https://docs.dagster.io/getting-started/create-new-project
- Create `cereal_processing` project
- install the project : `pip install -e .`

#### A Practical Sample 

  Dataset : [Cereal.csv](https://raw.githubusercontent.com/dagster-io/dagster/master/examples/docs_snippets/docs_snippets/intro_tutorial/cereal.csv) - put it in `data` folder next to `solids`

- **Step 1 :**  Single Solid Inside Our Project (*Load Cereals*) : https://docs.dagster.io/tutorial/intro-tutorial/single-solid-pipeline

  - `step_1.py`  (in `solids` folder): 

  ```python
  import csv
  import os
  
  from dagster import pipeline, solid
  
  @solid
  def hello_cereal(context):
      # Assumes the dataset is in the same directory as this Python file
      dataset_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), f"data{os.path.sep}cereal.csv")
      context.log.info(f"{dataset_path}")
      with open(dataset_path, "r") as fd:
          # Read the rows in using the standard csv library
          cereals = [row for row in csv.DictReader(fd)]
  
      context.log.info(f"Found {len(cereals)} cereals")
  ```

  add this solids to the pipeline & `dagit`

- **Step 2** : Connecting Solids (*Load -> Sort*) : https://docs.dagster.io/tutorial/intro-tutorial/connecting-solids
  
  - `Step_2.py`
  
  ```python
  import csv
  import os
  
  from dagster import execute_pipeline, pipeline, solid
  
  
  @solid
  def load_cereals(context):
      csv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), f"data{os.path.sep}cereal.csv")
      with open(csv_path, "r") as fd:
          cereals = [row for row in csv.DictReader(fd)]
  
      context.log.info(f"Found {len(cereals)} cereals".format())
      return cereals
  
  
  @solid
  def sort_by_calories(context, cereals):
      sorted_cereals = list(
          sorted(cereals, key=lambda cereal: cereal["calories"])
      )
  
      context.log.info(f'Most caloric cereal: {sorted_cereals[-1]["name"]}')
  
  
  ```
  
  - define a new pipeline file : `cereals_sort_by_colories.py` in `pipelines folder` 
  
  ```python
  from dagster import ModeDefinition, pipeline
  from cereal_processing.solids.step_2 import load_cereals, sort_by_calories
  
  MODE_DEV = ModeDefinition(name="dev", resource_defs={})
  MODE_TEST = ModeDefinition(name="test", resource_defs={})
  
  @pipeline(mode_defs=[MODE_DEV, MODE_TEST])
  def cereals_sort_by_colories():
      sort_by_calories(load_cereals())
  
  ```
  
  - add this new pipeline to `repository.py`
  
  ```python
    from dagster import repository
    
    from cereal_processing.pipelines.my_pipeline import my_pipeline
    from cereal_processing.pipelines.cereals_sort_by_colories import cereals_sort_by_colories
    from cereal_processing.schedules.my_hourly_schedule import my_hourly_schedule
    from cereal_processing.sensors.my_sensor import my_sensor
    
    
    @repository
    def cereal_processing():
        pipelines = [my_pipeline, cereals_sort_by_colories]
        schedules = [my_hourly_schedule]
        sensors = [my_sensor]
    
        return pipelines + schedules + sensors
    
  ```
  
- **Step 3  :** Test the pipeline : https://docs.dagster.io/tutorial/intro-tutorial/testable

  - run `pytest`

  - check out the `cereal_processing_tests` folder 

  - create new test file in `cereal_processing_tests\test_solids`

     `test_cereals.py` for **Step1 & Step2** : 
     
     ```python
     from dagster import execute_solid
     from cereal_processing.pipelines.my_pipeline import MODE_TEST
     from cereal_processing.solids.step_1 import hello_cereal
     from cereal_processing.solids.step_2 import sort_by_calories, load_cereals
     import csv, os
     
     
     def test_load_cereals():
     
         result = execute_solid(load_cereals, mode_def=MODE_TEST)
     
         assert result.success
         assert len(result.output_value()) == 77
     
     
     def test_sort_by_colories():
     
         csv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                                 f"data{os.path.sep}cereal.csv")
         with open(csv_path, "r") as fd:
             cereals = [row for row in csv.DictReader(fd)]
     
         result = execute_solid(sort_by_calories,input_values= {"cereals" : cereals}, mode_def=MODE_TEST)
         assert result.success
         assert result.output_value()[-1]["name"] == "Strawberry Fruit Wheats"
     
     ```

  - copy the `data` folder to `cereal_processing_tests ` folder

  - **correct return values**

  - add new file `test_step2.py` to the `test_pipelines` : 

    ```python
    from dagster import execute_pipeline
    from cereal_processing.pipelines.cereals_sort_by_colories import cereals_sort_by_colories
    
    
    def test_step2():
        result = execute_pipeline(cereals_sort_by_colories, mode="test")
    
        assert result.success
        assert len(result.output_for_solid("load_cereals")) == 77
        assert result.output_for_solid("sort_by_calories")[-1]["name"] == "Strawberry Fruit Wheats"
    
    
    ```

  - **Run :** `pytest `

- **Step 4** : Configuring Solids :  https://docs.dagster.io/tutorial/intro-tutorial/configuring-solids
  - in `step_4.py` we have :

  ```python
  import csv
  import os
  
  from dagster import execute_pipeline, pipeline, solid, OutputDefinition
  
  @solid(config_schema={"csv_name": str})
  def read_csv(context):
      csv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), f"data{os.path.sep}{context.solid_config['csv_name']}")
      with open(csv_path, "r") as fd:
          lines = [row for row in csv.DictReader(fd)]
  
      context.log.info(f"Found {len(lines)} lines".format())
      return lines
  
  @solid
  def sort_by_calories_step4(context, cereals):
      sorted_cereals = list(
          sorted(cereals, key=lambda cereal: cereal["calories"])
      )
  
      context.log.info(f'Most caloric cereal: {sorted_cereals[-1]["name"]}')
      return sorted_cereals
  ```

  - **Solid names must be unique in a repository !**
  - change `solid_config['csv_name']` to `solid_config["csv_name"]` and see the error . --> change it into correct state.
  - in the `pipelines` folder , create the file `step_4.py` :
  
  ```python
  from dagster import ModeDefinition, pipeline
  from cereal_processing.solids.step_4 import read_csv, sort_by_calories_step4
  
  MODE_DEV = ModeDefinition(name="dev", resource_defs={})
  MODE_TEST = ModeDefinition(name="test", resource_defs={})
  
  @pipeline(mode_defs=[MODE_DEV, MODE_TEST])
  def step4_pipeline():
      sort_by_calories_step4(read_csv())
  
  ```
  
  - add this pipeline to `repository.py`
  
  ```python
  from dagster import repository
  
  from cereal_processing.pipelines.my_pipeline import my_pipeline
  from cereal_processing.pipelines.step_4 import step4_pipeline
  from cereal_processing.pipelines.cereals_sort_by_colories import cereals_sort_by_colories
  from cereal_processing.schedules.my_hourly_schedule import my_hourly_schedule
  from cereal_processing.sensors.my_sensor import my_sensor
  
  
  @repository
  def cereal_processing():
      pipelines = [my_pipeline, cereals_sort_by_colories, step4_pipeline]
      schedules = [my_hourly_schedule]
      sensors = [my_sensor]
  
      return pipelines + schedules + sensors
  
  ```
  - `dagit` and see why you *can not run this pipeline*
  - Enter the config visually as illustrated in the *Nikamooz Film of this Session*
  
- **How To Specify Configs ?**

  - in **test files** or **python scripts** : 

    ```python
    run_config = {
            "solids": {"read_csv": {"config": {"csv_name": "cereal.csv"}}}
        }
    result = execute_pipeline(configurable_pipeline, run_config=run_config)
    
    ```

  - in **CLI** : 

    - create a yaml file , for example `run_config.yaml`

      ```yaml
      solids:
        read_csv:
          config:
            csv_name: "cereal.csv"
      ```

    - pass this file along with pipeline to the `dagster` command 
    
    ```bash
    $ dagster pipeline execute -f configurable_pipeline.py -c run_config.yaml
    ```
    
  - Using **Presets** : change `step_4.py`  pipeline into :
  
  ```python
  from dagster import ModeDefinition, pipeline, PresetDefinition
  from cereal_processing.solids.step_4 import read_csv, sort_by_calories_step4
  
  MODE_DEV = ModeDefinition(name="dev", resource_defs={})
  MODE_TEST = ModeDefinition(name="test", resource_defs={})
  
  @pipeline(mode_defs=[MODE_DEV, MODE_TEST] , preset_defs=[
      PresetDefinition(
                  name="Sample_Cereals_Test",
                  run_config={"solids": {"read_csv": {"config": {"csv_name": "cereal_test.csv"} } } },
                  mode='test'
              ),
                  PresetDefinition(
                  name="Sample_Cereals_Dev",
                  run_config={"solids": {"read_csv": {"config": {"csv_name": "cereal.csv"} } } },
                  mode='dev'
              )
      ]
  )
  def step4_pipeline():
      sort_by_calories_step4(read_csv())
  ```
  
  - check the UI & in `playground` tab, change `mode` & select corresponding config in `preset` select box.
  
- **Step 5 :**  A More Complex Pipeline :  https://docs.dagster.io/tutorial/intro-tutorial/connecting-solids#a-more-complex-dag

  - define `step_5.py` in solids : 

    ```python
    import csv
    import os
    
    from dagster import execute_pipeline, pipeline, solid, OutputDefinition
    
    @solid
    def load_cereals_step5(_):
        dataset_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), f"data{os.path.sep}cereal.csv")
        with open(dataset_path, "r") as fd:
            cereals = [row for row in csv.DictReader(fd)]
        return cereals
    
    
    @solid
    def sort_by_calories_step5(_, cereals):
        sorted_cereals = list(
            sorted(cereals, key=lambda cereal: cereal["calories"])
        )
        most_calories = sorted_cereals[-1]["name"]
        return most_calories
    
    
    @solid
    def sort_by_protein_step5(_, cereals):
        sorted_cereals = list(
            sorted(cereals, key=lambda cereal: cereal["protein"])
        )
        most_protein = sorted_cereals[-1]["name"]
        return most_protein
    
    
    @solid
    def display_results(context, most_calories, most_protein):
        context.log.info(f"Most caloric cereal: {most_calories}")
        context.log.info(f"Most protein-rich cereal: {most_protein}")
    
    ```

  - define `step_5` pipeline : 

    ```python
    from dagster import ModeDefinition, pipeline, PresetDefinition
    from cereal_processing.solids.step_5 import display_results, load_cereals_step5
    from cereal_processing.solids.step_5 import sort_by_calories_step5, sort_by_protein_step5
    
    MODE_DEV = ModeDefinition(name="dev", resource_defs={})
    MODE_TEST = ModeDefinition(name="test", resource_defs={})
    
    @pipeline(mode_defs=[MODE_DEV, MODE_TEST])
    def step5_pipeline():
        cereals = load_cereals_step5()
        display_results(
            most_calories=sort_by_calories_step5(cereals),
            most_protein=sort_by_protein_step5(cereals),
        )
    ```

    

  - Add this pipeline to the `repository.py`

  - `dagit`

  - add a pipeline test : `test_step5.py` 

    ```python
    from dagster import execute_pipeline
    from cereal_processing.pipelines.step_5 import step5_pipeline
    
    
    def test_step5():
        res = execute_pipeline(step5_pipeline, mode='test')
        assert res.success
        assert len(res.solid_result_list) == 4
        assert len(res.output_for_solid("load_cereals_step5")) == 77
        for solid_res in res.solid_result_list:
            assert solid_res.success
    
    
    ```
    
   - `pytest`

- **Step 6** : Dagster Types : https://docs.dagster.io/tutorial/advanced-tutorial/types
  
  - `step_6.py` solid : 
  
  ```python
  import csv
  import os
  
  from dagster import execute_pipeline, pipeline, solid, OutputDefinition
  from dagster import OutputDefinition, InputDefinition, DagsterType
  
  def is_list_of_dicts(_, value):
      return isinstance(value, list) and all(
          isinstance(element, dict) for element in value
      )
  
  
  SimpleDataFrame = DagsterType(
      name="SimpleDataFrame",
      type_check_fn=is_list_of_dicts,
      description="A naive representation of a data frame, e.g., as returned by csv.DictReader.",
  )
  
  @solid(config_schema={"csv_name": str}, output_defs=[OutputDefinition(name="cereals",dagster_type=SimpleDataFrame)])
  def read_csv_step_6(context):
      csv_name = context.solid_config["csv_name"]
      csv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), f"data{os.path.sep}{csv_name}")
      with open(csv_path, "r") as fd:
          lines = [row for row in csv.DictReader(fd)]
  
      context.log.info(f"Found {len(lines)} lines".format())
      return lines
  
  @solid(input_defs=[InputDefinition(name="cereals",dagster_type = SimpleDataFrame)])
  def sort_by_calories_step6(context, cereals):
      sorted_cereals = list(
          sorted(cereals, key=lambda cereal: cereal["calories"])
      )
  
      context.log.info(f'Most caloric cereal: {sorted_cereals[-1]["name"]}')
      return sorted_cereals
  ```
  
  - `step_6.py` pipeline : 
  
  ```python
  from dagster import ModeDefinition, pipeline, PresetDefinition
  from cereal_processing.solids.step_6 import read_csv_step_6, sort_by_calories_step6
  
  MODE_DEV = ModeDefinition(name="dev", resource_defs={})
  MODE_TEST = ModeDefinition(name="test", resource_defs={})
  
  @pipeline(mode_defs=[MODE_DEV, MODE_TEST] , preset_defs=[
      PresetDefinition(
                  name="Sample_Cereals_Test",
                  run_config={"solids": {"read_csv_step_6": {"config": {"csv_name": "cereal.csv"} } } },
                  mode='test'
              )
      ]
  )
  def step6_pipeline():
      sort_by_calories_step6(read_csv_step_6())
  
  ```
  
  - Add This pipeline to `repository.py`
  
    
  
- **Step 7** :  Scheduling Pipeline Runs : https://docs.dagster.io/tutorial/advanced-tutorial/scheduling
  - create `step_7.py` in `schedules` folder as follows : 

    ```python
    from datetime import datetime,time
    
    from dagster import daily_schedule, schedule
    
    
    @daily_schedule(
        pipeline_name="step4_pipeline",
        start_date=datetime(2021, 4, 20),
        execution_time=time(3, 45),
        execution_timezone="Asia/Tehran",
        mode='test'
    )
    def daily_cereals(date):
        run_config={"solids": {"read_csv": {"config": {"csv_name": "cereal.csv"} } } }
        return run_config
    
    @schedule(
        cron_schedule="*/2 * * * *",
        pipeline_name="step6_pipeline",
        execution_timezone="Asia/Tehran",
        mode='test'
    )
    def my_2minitues_cereal_check(context):
        date = context.scheduled_execution_time.strftime("%Y-%m-%d")
        run_config={"solids": {"read_csv_step_6": {"config": {"csv_name": "cereal.csv"} } } }
        return  run_config
    
    ```

    

  - update `repository.py`

    ```python
    
    @repository
    def cereal_processing():
        pipelines = [my_pipeline, cereals_sort_by_colories, step4_pipeline, step5_pipeline, step6_pipeline]
        schedules = [my_2minitues_cereal_check,daily_cereals]
        sensors = [my_sensor]
    
        return pipelines + schedules + sensors
    ```

    

  - create an empty `dagster.yaml` file in the root folder of the projects ([see a working example](https://github.com/OMR5221/dagster_docker/blob/0eab1365bcf7aa557ea553fcc9f41f24e321d76d/dagster.yaml))

  - in another console tab , activate the venv and run 

    ```bash
    dagster-daemon run
    ```

    

  - you need to **define an Environment Variable** called `DAGSTER_HOME` that points to ***dagster project folder*** .  

  - stop `dagit` , close the console,  open a new console (to read out the new Env Variables), go to Dagster Project folder , Activate Venv and run `dagit` again .

  - visit the Dagster UI & You must see the automated Runs!

    

- **Step 8 :**  Sensors : https://docs.dagster.io/concepts/partitions-schedules-sensors/sensors

  - define `step_8.py` sensor in sensors folder 

  ```python
  import os
  from dagster import sensor, RunRequest
  
  
  @sensor(pipeline_name="step4_pipeline", mode='test')
  def new_cereals(_context):
      csv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), f"data")
  
      for filename in os.listdir(csv_path):
          filepath = os.path.join(csv_path, filename)
          if os.path.isfile(filepath):
              yield RunRequest(
                  run_key=filename,
                  run_config={"solids": {"read_csv": {"config": {"csv_name": filename} } } }
              )
  ```

    - add this sensor to the `repository.py` file
    - restart `dagit` and `dagster-daemon` 
    - enable sensor in ui
    - make a copy of `cereal.csv`  in `data` folder 
    - check to see the result

- **Keep Learning :** 

  - Code Snippets (airflow, aws, docker , ...) : https://github.com/dagster-io/dagster/tree/0.11.6/examples/docs_snippets/docs_snippets
  - Resources & Modes : https://docs.dagster.io/tutorial/advanced-tutorial/pipelines
  - Materialization : https://docs.dagster.io/tutorial/advanced-tutorial/materializations
  - Workspace & Repositories : https://docs.dagster.io/tutorial/advanced-tutorial/repositories
  - Versioning : https://docs.dagster.io/guides/dagster/memoization
  - Executors : 
  
  | Name                                                         | Description                                            |
  | ------------------------------------------------------------ | ------------------------------------------------------ |
  | [Executing on Celery](https://docs.dagster.io/deployment/custom-infra/celery) | This guide explains how to execute Dagster on Celery.  |
  | [Executing on Dask](https://docs.dagster.io/deployment/custom-infra/dask) | This guide explains how to execute Dagster on Dask.    |
  | [Executing on Airflow](https://docs.dagster.io/deployment/custom-infra/airflow) | This guide explains how to execute Dagster on Airflow. |



