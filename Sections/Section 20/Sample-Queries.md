```sql
select name, formatReadableSize(sum(data_compressed_bytes)) as compressed,
		formatReadableSize(sum(data_uncompressed_bytes)) as uncompressed,
		sum(data_compressed_bytes)*100/sum(data_uncompressed_bytes) as compress_ratio 
from system.columns 
where table = 'daily_stats' group by name with totals order by sum(data_compressed_bytes);


SELECT table,
    formatReadableSize(sum(bytes)) as size,
    min(min_date) as min_date,
    max(max_date) as max_date
    FROM system.parts
    WHERE active
GROUP BY table;



SELECT table, formatReadableSize(size) as size, rows, days, formatReadableSize(avgDaySize) as avgDaySize FROM (
    SELECT
        table,
        sum(bytes) AS size,
        sum(rows) AS rows,
        min(min_date) AS min_date,
        max(max_date) AS max_date,
        (max_date - min_date) AS days,
        size / (max_date - min_date) AS avgDaySize
    FROM system.parts
    WHERE active
    GROUP BY table
    ORDER BY rows DESC
);


select parts.*, 
       columns.compressed_size, 
       columns.uncompressed_size, 
       columns.ratio 
from ( 
         select table, 
                formatReadableSize(sum(data_uncompressed_bytes))          AS uncompressed_size, 
                formatReadableSize(sum(data_compressed_bytes))            AS compressed_size, 
                sum(data_compressed_bytes) / sum(data_uncompressed_bytes) AS ratio 
         from system.columns 
         where database = currentDatabase() 
         group by table 
         ) columns 
         right join ( 
    select table, 
           sum(rows)                                            as rows, 
           max(modification_time)                               as latest_modification, 
           formatReadableSize(sum(bytes))                       as disk_size, 
           formatReadableSize(sum(primary_key_bytes_in_memory)) as primary_keys_size, 
           any(engine)                                          as engine, 
           sum(bytes)                                           as bytes_size 
    from system.parts 
    where database = currentDatabase() 
    group by table 
    ) parts on columns.table = parts.table 
order by parts.bytes_size desc;

--create view meta.tables_info as
select concat(database, '.', table)                         as table,
       formatReadableSize(sum(bytes))                       as size,
       sum(rows)                                            as rows,
       max(modification_time)                               as latest_modification,
       sum(bytes)                                           as bytes_size,
       any(engine)                                          as engine,
       formatReadableSize(sum(primary_key_bytes_in_memory)) as primary_keys_size
from system.parts
where active
group by database, table
order by bytes_size desc;

select name, formatReadableSize(sum(data_compressed_bytes)) as compressed, 
		formatReadableSize(sum(data_uncompressed_bytes)) as uncompressed, 
		sum(data_compressed_bytes)*100/sum(data_uncompressed_bytes) as compress_ratio  
from system.columns  
where table = 'daily_stats' group by name with totals order by sum(data_compressed_bytes);




SELECT case when match(hashtag, 
            '[Cc]orona.*|COVID.*|[Cc]ovid.*|[Cc]oVID_19.*|[Cc]orvid19.*|COVD19.*|CORONA.*|KILLTHEVI.*|SARSCoV.*|ChineseVi.*|WuhanVir.*|ChinaVir.*|[Vv]irus.*|
            [Qq]uarantine|[Pp]andemic.*|[Cc]linical[Tt]rial.*|FlattenTheCurve.*|SocialDistancing.*|StayHome.*|StayTheFHome.*|StayAtHome.*|stopthespread.*|
            SafeHands.*|WashYourHands.*|SelfIsolation.*')                           then 'COVID19' 
            when match(hashtag, '[Jj]anta[Cc]urfew.*|[Jj]anata[Cc]urfew.*')         then 'JantaCurfew'
            when match(hashtag, 'Bhula.*')                                          then 'Bhula'
            when match(hashtag, '[Ss]t[Pp]atrick.*|HappyStPatrick')                 then 'StPatricks day'
            when match(hashtag, '[Cc]hina.*')                                       then 'China'
            when match(hashtag, '[Ii]taly.*')                                       then 'Italy'
            when match(hashtag, '[Ii]ran.*')                                        then 'Iran'
            when match(hashtag, '[Ii]ndia.*')                                       then 'India'
            when match(hashtag, '[Hh]appy[Mm]others[Dd]ay.*|[Mm]others[Dd]ay.*')    then 'MothersDay'
            else hashtag END
            as Hashtag,
  SUM(CASE WHEN created >= '2020-05-14 00:00:00' AND created <= '2020-03-14 23:59:59' THEN 1 END) "May 14th'20",
  SUM(CASE WHEN created >= '2020-05-13 00:00:00' AND created <= '2020-03-13 23:59:59' THEN 1 END) "May 13th'20",
  SUM(CASE WHEN created >= '2020-05-12 00:00:00' AND created <= '2020-03-12 23:59:59' THEN 1 END) "May 12th'20"
FROM twitterDBhashtags
group by Hashtag 
order by "May 12th'20" DESC limit 20;

WITH 'f' AS ch 
SELECT　 
  arraySplit((x, i) -> x = ch and sequence[i - 1] != ch or x != ch and sequence[i - 1] = ch, sequence, arrayEnumerate(sequence)) parts, 
  arrayMap(part -> arrayMap((x, index) -> (x, x = ch ? index : 0), part, arrayEnumerate(part)), parts) parts_and_number, 
  arrayFlatten(parts_and_number) result 
FROM ( 
  SELECT arrayJoin([ 
    ['p','p','f','f','f','f','p','f', 'f', 'f'], 
    ['p','w','f','f','f','f','p','f', 'f', 'f'], 
    ['f','f','f','f','p','f', 'f', 'f'], 
    ['p','w'], 
    ['f', 'f'],   
    ['f'] 
  ]) as sequence) 
/* 
Row 1: 
────── 
parts:            [['p','p'],['f','f','f','f'],['p'],['f','f','f']] 
parts_and_number: [[('p',0),('p',0)],[('f',1),('f',2),('f',3),('f',4)],[('p',0)],[('f',1),('f',2),('f',3)]] 
result:           [('p',0),('p',0),('f',1),('f',2),('f',3),('f',4),('p',0),('f',1),('f',2),('f',3)] 
Row 2: 
────── 
parts:            [['p','w'],['f','f','f','f'],['p'],['f','f','f']] 
parts_and_number: [[('p',0),('w',0)],[('f',1),('f',2),('f',3),('f',4)],[('p',0)],[('f',1),('f',2),('f',3)]] 
result:           [('p',0),('w',0),('f',1),('f',2),('f',3),('f',4),('p',0),('f',1),('f',2),('f',3)] 
Row 3: 
────── 
parts:            [['f','f','f','f'],['p'],['f','f','f']] 
parts_and_number: [[('f',1),('f',2),('f',3),('f',4)],[('p',0)],[('f',1),('f',2),('f',3)]] 
result:           [('f',1),('f',2),('f',3),('f',4),('p',0),('f',1),('f',2),('f',3)] 
Row 4: 
────── 
parts:            [['p','w']] 
parts_and_number: [[('p',0),('w',0)]] 
result:           [('p',0),('w',0)] 
Row 5: 
────── 
parts:            [['f','f']] 
parts_and_number: [[('f',1),('f',2)]] 
result:           [('f',1),('f',2)] 
Row 6: 
────── 
parts:            [['f']] 
parts_and_number: [[('f',1)]] 
result:           [('f',1)] 
*/





SELECT 
['p','p','f','f','f','f','p','f', 'f', 'f'] AS sequence, 
arrayMap( (x,y) -> (x,  
   if (x='f', (arrayFirstIndex( k -> k=0, 
       arrayCumSumNonNegative((n, index) -> n = 'f' ? 1 : -index, 
       arrayReverse(arraySlice(sequence,1,y)) as arr, 
       arrayEnumerate(arr))) 
   )-1, 0)), sequence, arrayEnumerate(sequence)) 
result: 
[('p',0),('p',0),('f',1),('f',2),('f',3),('f',4),('p',0),('f',1),('f',2),('f',3)]

```

