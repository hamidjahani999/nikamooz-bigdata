import time, re
import datetime, sys
import requests, json
from elasticsearch import Elasticsearch, RequestError
from elasticsearch import helpers
from datetime import datetime

es = Elasticsearch(http_auth=('elastic', 'changeme'))



def main() :
    count_sofar = 1
    count_needed = 10000    
    start_time = datetime.now()
    print("Started ...")
    while count_sofar <= count_needed:
                test_doc = {"id":f"{count_sofar}", "content" : "sample text", "@timestamp": datetime.utcnow().date() }
            
                res = es.index(index="data-tweets-daily", id=count_sofar, body=test_doc , op_type="create")
                count_sofar += 1
                if count_sofar%10==0 :
                    print("-"*80)
                    print(str(count_sofar))
                    print(test_doc)
    lasts = datetime.now() - start_time
    print(f"Bulk Operation Took {lasts.seconds}:{lasts.microseconds} seconds")                    

def main_bulk() :
    count_sofar = 1
    count_needed = 10000    
    actions=[]
    start_time = datetime.now()
    print("Started ...")
    while count_sofar <= count_needed:
        action = {
                  "_index": "data-tweets-daily",
                  "_id": count_sofar,
                  "_op_type": "create", 
                  "_source": {
                        "id":f"{count_sofar}", 
                        "content" : "sample text",  
                        "@timestamp": datetime.utcnow().date()
                  
                  }
                  
        }
        actions.append(action)
        count_sofar += 1              
        if count_sofar%100==0 :
            print(str(count_sofar))
            helpers.bulk(es, actions)
            actions=[]
    lasts = datetime.now() - start_time
    print(f"Bulk Operation Took {lasts.seconds}:{lasts.microseconds} seconds")

if __name__ == '__main__':
    main()        
    # main_bulk()        
