# from confluent_kafka import Producer
from faker import Faker
import json
import datetime, decimal
import time, codecs
from faker.providers.person import Provider
from faker.providers.profile import Provider as ProfileProvider

def datetime_handler(x):
    if isinstance(x, datetime.datetime) or isinstance(x, datetime.date) : 
        # return "{}-{}-{}".format(x.year, x.month, x.day)
        date = datetime.datetime.strptime(str(x), '%Y-%m-%d')
        return f"{date.year}-{date.month}-{date.day}"
    elif isinstance(x, decimal.Decimal):
        return float(x)

# fake = Faker()
fake = Faker('fa_IR')

f = codecs.open("data_simple.txt","w",encoding="utf-8")
for i in range(10):
    data = {
        # 'name': fake.first_name(),
        # 'family' : fake.last_name(),
        'age': fake.random_int(min=18, max=80, step=1),
        'street': fake.address(),
        'city': fake.city(),
        'state': fake.state(),
        'profile' : fake.profile()
         # 'zip': fake.zipcode()
    }
    # print(data)
    m = json.dumps(data,ensure_ascii=False, default=datetime_handler)
    f.write(f"{m}\n")
f.close()
