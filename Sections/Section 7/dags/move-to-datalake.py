# 6: using Variables
# import them using Admin Panel and the config file Provided 

from airflow import DAG,utils
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator 

from airflow.models import Variable
from airflow import settings
from datetime import timedelta, date,datetime
import pandas as pd
import sys, glob

#--------------------------------------------------------------------------------

default_args = {
    "owner": "nikamooz",
    "depends_on_past": False,
    "start_date": datetime(2020, 2, 13),
    "email": ["smbanaei@ut.ac.ir"],
    "retries": 1,
    "retry_delay": timedelta(minutes=1),
    'provide_context' : True,

}


STAGE2="/home/mojtaba/data/stage/step2"
LAKE="/home/mojtaba/data/lake"

      



dag = DAG("Move-To-Datalake", default_args=default_args, schedule_interval="0 * * * *" , catchup=False , template_searchpath='/home/mojtaba/data/scripts');


#------------------------------------ Python Functions---------------------------------------


def convert_files_to_parquet(**kwargs) : 
    flist= glob.glob(f"{STAGE2}/*.csv")
    for i in flist :
        df = pd.read_csv(i, header=None , names=['id','sendTime','sendTimePersian', 'senderName', 'senderUsername', 'type', 'content' ],dtype={'content': object})
        df.to_parquet( f"{LAKE}/{i.split('/')[-1].split('.')[0]}.parquet")
                      

convert_to_parquet = PythonOperator(
    task_id='Convert-Hourly-CSV-To-Parquet',
    python_callable=convert_files_to_parquet,
    dag=dag,
)

combine_csv_files = BashOperator(
    task_id='Combine-CSV-Files',
    bash_command= "combine_csv.sh" ,
    # bash_command='curl --retry 10 --output {0} -L -H "User-Agent:Chrome/61.0" --compressed "http://members.tsetmc.com/tsev2/excel/MarketWatchPlus.aspx?d=0"'.format( path.join(EXCEL_FILE_PATH, "{0}_{1}.{2}".format(EXCEL_FILE_NAME, date.today().strftime("%Y_%m_%d"),EXCEL_FILE_EXT))),
    dag=dag,
)

#  ----------------------- DAG Structure -------------------------------

combine_csv_files >> convert_to_parquet

#--------------------------------------------------------------------------



