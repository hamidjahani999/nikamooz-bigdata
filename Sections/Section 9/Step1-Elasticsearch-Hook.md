#### Airflow Elasticsearch Hook

1- Setup **Airflow /Elasticsearch** Cluster

```bash
$ docker-compose -f ./docker-compose-elasticsearch.yml up
```
2-create `tweets` index and insert some data  into it.

- check ‍‍‍‍`localhost:9200`

- go to **Kibana** dashboard : localhost:5601

- from main menu, open `Dev Tools` and run 

  ```json
  # index a doc
  PUT tweets/_doc/1
  {
    "id" : 123 ,
    "content": "دوره مهندسی داده" ,
    "date" : "1399/11/05"
  }
  
  PUT tweets/_doc/2
  {
    "id" : 143 ,
    "content": "داده های شبکه اجتماعی" ,
    "date" : "1399/11/05"
  }
  
  PUT tweets/_doc/1
  {
    "id" : 123 ,
    "content": "دوره مبانی مهندسی داده" ,
    "date" : "1399/11/04"
  }
  
  GET tweets/_doc/1
  
  GET _search
  {
    "query": {
      "match_all": {}
    }
  }
  
  ```





#### 3. Add `elasticsearch` to workers python Libs 

- create new `Dockerfile` & add  these libraries to `pip install section`

```bash
elasticsearch
apache-airflow-providers-slack
psycopg2
python-mysqldb
```



- build `Dockerfile` and name it : `nikamooz/airflow:elasticsearch`

```bash
$ docker build -t nikamooz/airflow:elasticsearch .
```

- create .env file and add this line to it :

```yaml
AIRFLOW_IMAGE_NAME=nikamooz/airflow:elasticsearch
```

  

- Copy `stpes/1` to `dags` & `plugins` folders 

- Set :  `AIRFLOW__CORE__RELOAD_ON_PLUGIN_CHANGE: 'true'` in `docker-compose` file .

- Create `elasticsearch_default` connection
   - name : 	`elasticsearch_default` 
   - Conn Type : `elasticsearch`
   - Host : `elastic`
   - Schema : `tweets`
   - port : `9200`
   
   

> ***dag & plugins* are in the `Python Path`  so every file/folder in them are accessible from within the `airflow`**   and imported automatically.