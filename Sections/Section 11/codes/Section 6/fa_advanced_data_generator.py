from faker import Faker
import json
import time, codecs
from faker.providers.person import Provider
from faker.providers.company import Provider as CompanyProvider
import datetime
# import date
import decimal

fake = Faker('fa_IR')

f = codecs.open("data.txt","w",encoding="utf-8")

def datetime_handler(x):
    if isinstance(x, datetime.datetime) or isinstance(x, datetime.date) : 
        # return "{}-{}-{}".format(x.year, x.month, x.day)
        date = datetime.datetime.strptime(str(x), '%Y-%m-%d')
        return f"{date.year}-{date.month}-{date.day}"
    elif isinstance(x, decimal.Decimal):
        return float(x)

    
    

def generate_participants():
    return { "id": fake.pyint(0, 100), "name": fake.name(), "email": fake.email() }

def generate_event(**kwargs):
    res = {
        "event_name": fake.bs(),
        "start_date": fake.date_between('now', '+1y'),
        "participants": [generate_participants() for _ in range(fake.pyint(1, 5))],
        "status": fake.random_element(elements=("Active", "Cancelled")),
        "website": fake.uri(),
        "address": fake.address()
    }
    res.update(kwargs)
    return res

data = generate_event()

    # print(data)
m = json.dumps(data,ensure_ascii=False, default=datetime_handler)
f.write(f"{m}\n")
f.close()