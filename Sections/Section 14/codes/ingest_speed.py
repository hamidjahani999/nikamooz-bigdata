import time, re
import datetime, sys
import requests, json
from elasticsearch import Elasticsearch, RequestError
from elasticsearch import helpers
from datetime import datetime

es = Elasticsearch(http_auth=('elastic', 'changeme'))



def main() :
    count_sofar = 0
    count_needed = 5000    
    start_time = datetime.now()
    print("Started ...")
    while count_sofar < count_needed:
                test_doc = {"id":count_sofar, "content" : "sample text", "timestamp": datetime.utcnow()}
                res = es.index(index="speed_test", id=count_sofar, body=test_doc)
                if res :
                    count_sofar += 1
                if count_sofar%10==0 :
                    print(str(count_sofar))
    lasts = datetime.now() - start_time
    print(f"Bulk Operation Took {lasts.seconds}:{lasts.microseconds} seconds")                    

def main_bulk() :
    count_sofar = 0
    count_needed = 5000    
    actions=[]
    start_time = datetime.now()
    print("Started ...")
    while count_sofar < count_needed:
        action = {
                  "_index": "speed_test_bulk",
                  "_id": count_sofar,
                  "_source": {
                        "id":count_sofar, 
                        "content" : "sample text",  
                        "@timestamp": datetime.utcnow()
                  }
        }
        actions.append(action)
        count_sofar += 1              
        if count_sofar%100==0 :
            print(str(count_sofar))
            helpers.bulk(es, actions)
            actions=[]
    lasts = datetime.now() - start_time
    print(f"Bulk Operation Took {lasts.seconds}:{lasts.microseconds} seconds")

if __name__ == '__main__':
    # main()        
    main_bulk()        
