import codecs, json, sys
import re

sys.stdin.reconfigure(encoding='utf-8')
tweet = json.load(sys.stdin)
hlist=re.findall(r"#(\w+)", tweet["content"])
tweet["Hash_Tags"] = hlist
json.dump(tweet,sys.stdout,ensure_ascii=False)
# sys.stdout.write(",".join(hlist))
