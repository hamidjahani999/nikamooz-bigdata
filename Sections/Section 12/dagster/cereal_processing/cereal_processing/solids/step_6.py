import csv
import os

from dagster import execute_pipeline, pipeline, solid, OutputDefinition
from dagster import OutputDefinition, InputDefinition, DagsterType

def is_list_of_dicts(_, value):
    return isinstance(value, list) and all(
        isinstance(element, dict) for element in value
    )


SimpleDataFrame = DagsterType(
    name="SimpleDataFrame",
    type_check_fn=is_list_of_dicts,
    description="A naive representation of a data frame, e.g., as returned by csv.DictReader.",
)

@solid(config_schema={"csv_name": str}, output_defs=[OutputDefinition(name="cereals",dagster_type=SimpleDataFrame)])
def read_csv_step_6(context):
    csv_name = context.solid_config["csv_name"]
    csv_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), f"data{os.path.sep}{csv_name}")
    with open(csv_path, "r") as fd:
        lines = [row for row in csv.DictReader(fd)]

    context.log.info(f"Found {len(lines)} lines".format())
    return  lines

@solid(input_defs=[InputDefinition(name="cereals",dagster_type = SimpleDataFrame)])
def sort_by_calories_step6(context, cereals): 
    sorted_cereals = list(
        sorted(cereals, key=lambda cereal: cereal["calories"])
    )

    context.log.info(f'Most caloric cereal: {sorted_cereals[-1]["name"]}')
    return sorted_cereals