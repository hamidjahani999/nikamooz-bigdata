#### Try it yourself!

- Check out the Elastic Example Repository : [elastic/examples: Home for Elasticsearch examples available to everyone. It's a great way to get started. (github.com)](https://github.com/elastic/examples)
  - **Video :** [examples/Machine Learning/Getting Started Examples/server_metrics at master · elastic/examples (github.com)](https://github.com/elastic/examples/tree/master/Machine Learning/Getting Started Examples/server_metrics)
  - [examples/Graph/movie_recommendations at master · elastic/examples (github.com)](https://github.com/elastic/examples/tree/master/Graph/movie_recommendations)
  - [examples/Exploring Public Datasets/nyc_restaurants at master · elastic/examples (github.com)](https://github.com/elastic/examples/tree/master/Exploring Public Datasets/nyc_restaurants)
  - [examples/Exploring Public Datasets/nyc_traffic_accidents at master · elastic/examples (github.com)](https://github.com/elastic/examples/tree/master/Exploring Public Datasets/nyc_traffic_accidents)
- **Video** : [Kibana: Getting Started with Visualizations | Elastic Videos](https://www.elastic.co/webinars/kibana-101-get-started-with-visualizations)

