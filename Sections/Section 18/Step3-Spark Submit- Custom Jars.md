#### Step3 : Submit a sample Python App

- uncomment `postgres` service in `docker-compose.yml` and start it 

  -  `docker-compose up -d`

  

- using `dbeaver` and `user/password` provided in `docker-compose` file,  create this table (run `sql script`)

  ```sql
  
  CREATE TABLE public.meta_reports (
  	latitude float4 null,
  	longitude float4 null,
  	time_received varchar(15) null,
  	vehicle_id int8 null,
  	distance_along_trip float4 null,
  	inferred_direction_id int null,
  	inferred_phase varchar(20),
  	inferred_route_id varchar(20),
  	inferred_trip_id varchar(20),
  	next_scheduled_stop_distance float4 null,
  	next_scheduled_stop_id varchar(20),
  	report_hour varchar(15),
  	report_date varchar(15)
  );
  ```

  

- run `step3-submit-apps` notebook to verify that everything is ok 

- download the python file (`File->Download As`) / copy it to `app` folder (`/opt/spark-apps`)

- got to `pyspark` container 

  - ` docker exec -it pyspark bash`

- sumbit the python app as follows :

  ```bash
  $ unset PYSPARK_DRIVER_PYTHON
  $ /opt/spark/bin/spark-submit --master spark://spark-master:7077 \
  --jars /opt/spark-apps/postgresql-42.2.22.jar \
  --driver-memory 500M \
  --executor-memory 500M \
  /opt/spark-apps/step3-submit-apps.py
  
  $ export PYSPARK_DRIVER_PYTHON=python
  ```

  

