

## [Set up a data stream](https://github.com/elastic/elasticsearch/edit/7.13/docs/reference/data-streams/set-up-a-data-stream.asciidoc)

To set up a data stream, follow these steps:

- [Step 1. Create an index lifecycle policy](https://www.elastic.co/guide/en/elasticsearch/reference/current/set-up-a-data-stream.html#create-index-lifecycle-policy)
- [Step 2. Create component templates](https://www.elastic.co/guide/en/elasticsearch/reference/current/set-up-a-data-stream.html#create-component-templates)
- [Step 3. Create an index template](https://www.elastic.co/guide/en/elasticsearch/reference/current/set-up-a-data-stream.html#create-index-template)
- [Step 4. Create the data stream](https://www.elastic.co/guide/en/elasticsearch/reference/current/set-up-a-data-stream.html#create-data-stream)
- Check Out the indices

#### Step1 : Create an index lifecycle policy

- create a Index Lifecycle Policy
- To create an index lifecycle policy in Kibana, open the main menu and go to **Stack Management > Index Lifecycle Policies**. Click **Create policy**

```json
PUT _ilm/policy/tweets-policy
{
  "policy": {
    "phases": {
      "hot": {
        "min_age": "0ms",
        "actions": {
          "set_priority": {
            "priority": 499
          },
          "rollover": {
            "max_size": "100kb",
            "max_docs": 100,
            "max_age": "30d"
          }
        }
      },
      "warm": {
        "min_age": "30d",
        "actions": {
          "forcemerge": {
            "max_num_segments": 1
          },
          "set_priority": {
            "priority": 400
          },
          "shrink": {
            "number_of_shards": 1
          }
        }
      },
      "cold": {
        "min_age": "60d",
        "actions": {
          "set_priority": {
            "priority": 300
          }
        }
      },
      "delete": {
        "min_age": "365d",
        "actions": {
          "delete": {
            "delete_searchable_snapshot": true
          }
        }
      }
    }
  }
}
```



#### Step2 : Create component templates

- A data stream requires a matching **index template**. In most cases, you compose this **index template using one or more component templates**
- To create a component template in Kibana, open the main menu and go to **Stack Management > Index Management**. In the **Index Templates** view, click **Create component template**
- Use the [Elastic Common Schema (ECS)](https://www.elastic.co/guide/en/ecs/1.8) when mapping your fields.

```json
# Creates a component template for mappings
PUT _component_template/tweets-mappings
{
  "template": {
    "mappings": {
      "properties": {
        "id" : {
          "type"  : "keyword"
        },
          "@timestamp": {
          "type": "date",
          "format": "date_optional_time||epoch_millis"
        },
        "content": {
          "type": "text"
        }
          
      }
    }
  },
  "_meta": {
    "description": "Mappings for @timestamp , id and content fields",
    "my-custom-meta-field": "More arbitrary metadata"
  }
}

# Creates a component template for index settings
PUT _component_template/tweets-settings
{
  "template": {
    "settings": {
       "index.lifecycle.name": "tweets-policy",
       "number_of_replicas": 1,
  	   "number_of_shards": 2,
	   "index.lifecycle.rollover_alias": "daily-tweets" 
    }
  },
  "_meta": {
    "description": "Settings for ILM",
    "my-custom-meta-field": "More arbitrary metadata"
  }
}
```

#### Step3 : Create an Index Template

- Use your component templates to create an index template
- use this schema for naming the index pattern : `<type>-<dataset>-<namespace>`
- That the template must be **data stream enabled**.
- A priority higher than `200` to avoid collisions with built-in templates.
- To create an index template in Kibana, open the main menu and go to **Stack Management > Index Management**. In the **Index Templates** view, click **Create template**.

```json
PUT _index_template/tweets-template
{
  "index_patterns": ["data-tweets-*"],
  "data_stream": { },
  "composed_of": [ "tweets-mappings", "tweets-settings" ],
  "priority": 500,
  "_meta": {
    "description": "Template for my time series data",
    "my-custom-meta-field": "More arbitrary metadata"
  }
}
```



#### Step4 : Create the Data Stream

- ingest some data

```bash
$ python .\ingest_data_stream.py
```

- check out the data stream in action (data-stream/backing indices/rollover)

  - rollover manually :

     ```bash
     $ POST /data-tweets-daily/_rollover/
     ```

  - the name is set in ingestion python code.

- stop and restart the  above code :

  - you got Error!
  - data streams can only be appended.

- search the data stream 

  ```json
  GET /data-tweets-daily/_search
  {
    
    "query": {
      "term": {
        "id": {
          "value": "5"
        }
      }
    }
    
  }
  ```

  

  
