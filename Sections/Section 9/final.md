

#### design a Dag for deleting airflow  history tables.

**Setting up SMTP Server for Airflow Email alerts using Gmail**:

Create an email id from which you want to send alerts about DAG failure or if you want to use **EmailOperator**. Edit `airflow.cfg` file to edit the smtp details for the mail server.

Create a google App Password for your gmail account. 

1. Visit your [App passwords](https://security.google.com/settings/security/apppasswords) page. You may be asked to sign in to your Google Account.
2. At the bottom, click **Select app** and choose the app you’re using.
3. Click **Select device** and choose the device you’re using.
4. Select **Generate**.
5. Follow the instructions to enter the App password (the 16 character code in the yellow bar) on your device.
6. Select **Done**.

Once you are finished, you won’t see that App password code again. However, you will see a list of apps and devices you’ve created App passwords for.

Edit `airflow.cfg` and edit the `[smtp]` section as shown below:

```py
[smtp]
smtp_host = smtp.gmail.com
smtp_starttls = True
smtp_ssl = False
smtp_user = YOUR_EMAIL_ADDRESS
smtp_password = 16_DIGIT_APP_PASSWORD
smtp_port = 587
smtp_mail_from = YOUR_EMAIL_ADDRESS
```

Edit the below parameters to the corresponding values:

`YOUR_EMAIL_ADDRESS` = Your Gmail address
`16_DIGIT_APP_PASSWORD` = The App password generated above



