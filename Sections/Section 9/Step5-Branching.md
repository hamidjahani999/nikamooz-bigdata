##### Branching DAG

---

![image-20210307192350580](dag_branching.png)

- copy  `steps/5` dags folder to main `dags` folder .

- checkout the `branch_dag` code :

```python 
def check_for_activated_source(**kwargs):
	ti = kwargs['ti']
	return ti.xcom_pull(task_ids='xcom_task').lower()

branch_task 	= BranchPythonOperator(task_id='branch_task', python_callable=check_for_activated_source, provide_context=True)

mysql_task 	    = BashOperator(task_id='mysql', bash_command='echo "MYSQL is activated"')
postgresql_task = BashOperator(task_id='postgresql', bash_command='echo "PostgreSQL is activated"')
s3_task 	    = BashOperator(task_id='s3', bash_command='echo "S3 is activated"')
mongo_task 	    = BashOperator(task_id='mongo', bash_command='echo "Mongo is activated"')
end_task 	    = DummyOperator(task_id='end_task', trigger_rule=TriggerRule.ONE_SUCCESS)
```



##### Trigger Rules

---

ALL_SUCCESS = 'all_success'
ALL_FAILED = 'all_failed'
ALL_DONE = 'all_done'
ONE_SUCCESS = 'one_success'
ONE_FAILED = 'one_failed'


```python 
end_task    = DummyOperator(task_id='end_task', trigger_rule=TriggerRule.ONE_SUCCESS)
```

- trigger the `branch_dag`
- check out the `XCom `
- trigger the `branch_dag_skipping`